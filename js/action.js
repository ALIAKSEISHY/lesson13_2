// поля, свойства (ключ: значение)

let tasks = [
    {
        isComplete: false,
        id: 1,
        name: 'Купить хлеб'
    },
    {
        isComplete: false,
        id: 2,
        name: 'Убрать в комнате'
    },
    {
        isComplete: true,
        id: 3,
        name: 'Пообедать'
    },
    {
        isComplete: false,
        id: 4,
        name: 'Выгулять собаку'
    },
    {
        isComplete: false,
        id: 5,
        name: 'Помыть собаку'
    },
    {
        isComplete: false,
        id: 6,
        name: 'Поужинать'
    },

];

console.log(tasks[1].name);


function drawTask(task) {
    return `
    <div class="task">
        <input type="checkbox" class="task__complete" ${ task.isComplete ? 'checked' : ''}>
        <span class="task__number">${task.id}</span>
        <span class="task__name">${task.name}</span>
    </div>`; //
}

const list = document.querySelector('.container__list');

/*tasks.forEach((item) => {
    list.innerHTML += drawTask(item);
});*/


//console.log(task_1.name);

//ДОМАШКА СДЕЛАТЬ МАССИВ ПОЛЬЗОВАТЕЛЕЙ
//у каждого пользователя будут: id; name; age; email, image*
//лучше сделать по второму способу (но сначала хоть по первому)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//ВТОРОЙ СПОСОБ (ПРАВИЛЬНЫЙ)

function createTaskTag(task) {
    const name = document.createElement('span'); // name = <span></span>
    name.className = 'task__name'; // name = <span class="task__name"></span>
    name.innerText = task.name; // name = <span class="task__name">Выгулять собаку</span>

    const number = document.createElement('span');
    number.className = "task__number";
    number.innerText = task.id;

    const checkbox = document.createElement('input'); //<input type="text">
    checkbox.type = 'checkbox'; //<input type="checkbox">
    checkbox.className = 'task__complete';
    checkbox.checked = task.isComplete;

    const deleteBtn = document.createElement('div');
    deleteBtn.className = 'task__delete'
    deleteBtn.innerText = 'x'; //<div class="task__delete id=""">x</div>
    deleteBtn.id = 'task_' + task.id;
    deleteBtn.addEventListener('click', itemClick);

    const taskTag = document.createElement('div');
    taskTag.className = 'task'; // taskTag = <div class="task"></div>
    taskTag.appendChild(checkbox); // taskTag = <div class="task"><input ...> </div>  //appendChild - вкладывает внутрь
    taskTag.appendChild(number); //taskTag = <div class="task"><input ...> <span> </div>
    taskTag.appendChild(name);
    taskTag.appendChild(deleteBtn);
    

    return taskTag;
}


drawTasks();

function drawTasks() {
    list.innerHTML = '';
    tasks.forEach(item => {
    const myTask = createTaskTag(item);
    list.appendChild(myTask); // list.innerHTML += myTask;
    });
}

//Events - События

const button = document.querySelector('#sayHello') // id sayHello на кнопке button;

function itemClick(event){//event - объект с инфо о событии (поле target содержит объект которым было вызвано событие)
    const taskIdString = event.target.id; //'task_3' через поле target события event в taskIdString записали id
    const taskId = +taskIdString.split('_')[1]; // ['task', '3'] обращаемся к элементу массива с индексом [1] (ко второму, к цифре)    + в начале преобразует строку в число или Number()
    // split() -> превращает строку в массив по разделителю =>
    // самый! лучший! день  split('!') => ['самый' , 'лучший'б 'день']

    /*const task = tasks.find(item => item.id === taskId);
    const index = tasks.indexOf(task);
    tasks.splice(index, 1);*/

    tasks = tasks.filter(item => item.id !== taskId);
    drawTasks();
    
    //button.removeEventListener('click', itemClick);//отписываемся от события после первого запуска функции
}

    //const arr = [2, 4, 6, 1, 0];
    //arr indexOf(1); => 3

//button.addEventListener('click', itemClick); //параметры: название_события, callback_function dblclick - название события двойного щелчка мыши и др.

console.log(button);


///////////////////////////

function addTask() {
    const input = document.querySelector('#taskName'); //забрали из input
    const taskName = input.value;
    const newTask = {
        isComplete: false,
        name: taskName,
        id: getNewId() // max id +1
    };
    tasks.push(newTask);
    input.value = '';
    drawTasks();

   
}

function getNewId() {
    /*let max = 0;
    tasks.forEach(item => {
        if (item.id > max)
        {
            max=item.id;
        }
    
    });*/

   /* for (let i=0; i<tasks.length; i++) {
        if (tasks[i].id > max) {
            max = tasks[i].id;
        }
    }*/

    const ids = tasks.map(item => item.id); //[1,2,3,4,5]
    const max = Math.max(...ids); //5
    return max +1;
    
}
console.log(getNewId());

const addButton = document.querySelector('#add');
addButton.addEventListener('click', addTask);